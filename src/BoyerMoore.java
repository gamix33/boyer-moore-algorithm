import java.util.HashMap;

public class BoyerMoore {

    private String text;
    private int textSize;

    public static class NoMatchException extends Exception{
        private String message;

        public NoMatchException(String message){
            super();
            this.message = message;
        }

        @Override
        public String getMessage() {
            return message;
        }
    }

    public BoyerMoore(String text) {
        this.text = text;
        this.textSize = text.length();
    }

    private int min(int j, int i) {
        return j <= i ? j : i;
    }

    private int findPattern(int startIndex, String pattern) throws NoMatchException {
        if(startIndex > this.textSize - 1){
            throw new NoMatchException("Start index higher than text length.");
        }

        int patternSize = pattern.length();

        HashMap<Character, Integer> last = new HashMap<>();
        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        for(int i = 0; i < alphabet.length; i++){
            char a = alphabet[i];
            last.put(a, pattern.lastIndexOf(a));
        }

        /*System.out.println("last array: ");
        last.keySet().forEach(k -> {
            System.out.print(k + "=" + last.get(k) + " ");
        });
        System.out.println();*/

        int i, j, k;
        i = patternSize - 1 + startIndex;
        j = patternSize - 1;

        do {
            if (this.text.charAt(i) == pattern.charAt(j)) {
                if (j == 0) {
                    return i;
                } else {
                    i -= 1;
                    j -= 1;
                }
            } else {
                try{
                    k = last.get(this.text.charAt(i));
                }catch (NullPointerException e){
                    k = -1;
                }

                i = i + patternSize - min(j, 1 + k);
                j = patternSize - 1;
            }
        } while (i < this.textSize - 1);

        throw new NoMatchException("Pattern `" + pattern + "` not found.");
    }

    private boolean isPunctuation(char c){
        return c == ' ' || c == '\n' || c == '.' || c == ',';
    }

    private String completeWord(int index) {
        String word = "";

        if(index >= 0 && index < this.textSize){
            int i = index;
            while(i > 0 && !isPunctuation(this.text.charAt(i))){
                i--;
            }
            i++;
            while (i < this.textSize && !isPunctuation(this.text.charAt(i))){
                word += this.text.charAt(i);
                i++;
            }
        }

        return word;
    }

    public HashMap<Integer, String> findAllOccurences(String pattern) {
        HashMap<Integer, String> results = new HashMap<>();

        int index;
        int i = 0;

        long startTime = System.currentTimeMillis();

        try{
            while(i < this.textSize - 1){
                index = findPattern(i, pattern);

                results.put(index, completeWord(index));
                i = index + 1;
            }
        } catch (NoMatchException e) {
            //System.out.println("Not found from index " + i + ".");
        }

        long searchTime = System.currentTimeMillis() - startTime;
        System.out.println(String.format("\nsearch time: %dms", searchTime));

        return results;
    }
}
