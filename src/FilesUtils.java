import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FilesUtils {

    public static String readFile(String fileName){
        String fileContent = "";
        int data;

        try (InputStream in = new FileInputStream(fileName)) {
            System.out.println("reading file `" + fileName + "` ...");

            while ((data = in.read()) != -1) {
                fileContent += ((char) data);
            }
        } catch (IOException e) {
            System.out.println("FILE READ ERROR " + fileName);
        }

        return fileContent;
    }

}
