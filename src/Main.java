import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        String textFile = "seneca.txt";
        String text = FilesUtils.readFile(textFile);

        BoyerMoore boyerMoore = new BoyerMoore(text/*.toLowerCase()*/);

        String pattern = "omne";
        HashMap<Integer, String> occurences = boyerMoore.findAllOccurences(pattern);
        System.out.println(String.format("\n%d occurences of `%s` found:\n", occurences.size(), pattern));
        occurences.keySet().forEach(index -> {
            System.out.println(String.format("index: %d, word: %s", index, occurences.get(index)));
        });
    }
}
